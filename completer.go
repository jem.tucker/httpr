package httpr

import (
	"strings"
)

type completer struct {
	h HTTPer
}

func (c completer) Do(line []rune, pos int) (newLine [][]rune, length int) {
	// Readline will pass the whole line and current offset here
	// Completer needs to pass all the candidates, and how long they shared
	// the same characters in line
	// Example:
	//   [go, git, git-shell, grep]
	//   Do("g", 1) => ["o", "it", "it-shell", "rep"], 1
	//   Do("gi", 2) => ["t", "t-shell"], 2
	//   Do("git", 3) => ["", "-shell"], 3

	// Sack off everything post pos
	prepos := []rune{}
	for i, r := range line {
		if i > pos {
			break
		}

		prepos = append(prepos, r)
	}

	// Work out where we're at
	prefix := ""
	words := strings.Fields(string(prepos))
	if len(words) > 0 && pos > 0 && line[pos-1] != ' ' {
		prefix = words[len(words)-1]
	}

	suggs, err := c.h.Complete(string(prepos))
	if err != nil {
		return nil, 0
	}

	var output [][]rune
	for _, w := range suggs {
		if strings.HasPrefix(w, prefix) {
			output = append(output, []rune(strings.TrimPrefix(w, prefix)))
		}
	}

	if len(output) == 1 && prefix != "" && string(output[0]) == "" {
		output = [][]rune{[]rune(" ")}
	}

	return output, len(prefix)
}
