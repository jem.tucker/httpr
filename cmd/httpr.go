package main

import (
	"gitlab.com/jem.tucker/httpr"
	"gitlab.com/jem.tucker/httpr/action"
)

func main() {
	h := httpr.New(
		httpr.WithActions(
			action.Requestor,
			action.Exit,
			action.Date,
		),
	)

	httpr.Repl(h)
}
