package action

import (
	"context"
	"os"

	"gitlab.com/jem.tucker/httpr"
)

// Exit will terminate the program with an exit code of 0
var Exit = httpr.Action{
	Name:        "exit",
	Description: "exit httpr",
	Execute:     exit,
}

func exit(ctx context.Context, args []string) (string, error) {
	os.Exit(0)

	return "", nil
}
