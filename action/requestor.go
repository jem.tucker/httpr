package action

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/jem.tucker/httpr"
	"gitlab.com/jem.tucker/httpr/internal/stringlist"
)

// Requestor is an action that can perform http requests
var Requestor = httpr.Action{
	Name:        "http",
	Description: "pake an http request",
	Execute:     requestor,
	Complete:    requestorComplete,
}

func requestor(ctx context.Context, args []string) (string, error) {
	if len(args) < 2 {
		return "", errors.New("invalid arguments")
	}

	method := strings.ToUpper(args[0])
	url := args[1]

	req, err := http.NewRequestWithContext(ctx, method, url, nil)
	if err != nil {
		return "", err
	}

	rsp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return "", fmt.Errorf("read body: %w", err)
	}

	return fmt.Sprintf(
		"%s %s %s\n\n%s",
		rsp.Proto,
		rsp.Request.Method,
		rsp.Status,
		string(body),
	), nil
}

func requestorComplete(cmd []string) ([]string, error) {
	methods := stringlist.StringList{
		http.MethodGet,
		http.MethodHead,
		http.MethodPost,
		http.MethodPut,
		http.MethodPatch,
		http.MethodDelete,
		http.MethodConnect,
		http.MethodOptions,
		http.MethodTrace,
	}

	switch len(cmd) {
	case 0:
		return methods, nil

	case 1:
		matches := func(s string) bool {
			return strings.HasPrefix(s, cmd[0])
		}

		upper := methods.Select(matches)
		if len(upper) > 0 {
			return upper, nil
		}

		lower := methods.Map(func(s string) string {
			return strings.ToLower(s)
		}).Select(matches)
		return lower, nil

	case 2:
		token := strings.ToLower(cmd[1])
		https := "https://"
		if strings.HasPrefix(https, token) {
			return []string{https}, nil
		}
	}

	return nil, nil
}
