package action

import (
	"context"
	"time"

	"gitlab.com/jem.tucker/httpr"
)

// Date prints the current date and time
var Date = httpr.Action{
	Name:        "date",
	Description: "print the date and time",
	Execute: func(c context.Context, s []string) (string, error) {
		return time.Now().Format("Mon Jan 2 15:04:05"), nil
	},
}
