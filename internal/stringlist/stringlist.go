package stringlist

// StringList is a slice of strings
type StringList []string

// Select filters the StringList returning only those that match the given
// predicate
func (l StringList) Select(pred func(string) bool) StringList {
	var out StringList
	for _, s := range l {
		if pred(s) {
			out = append(out, s)
		}
	}
	return out
}

// Map iterates over a StringList generating a new StringList with func f applied
// to every string within it
func (l StringList) Map(f func(string) string) StringList {
	var out StringList
	for _, s := range l {
		out = append(out, f(s))
	}
	return out
}
