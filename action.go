package httpr

import (
	"context"
)

// ExecuteFunc is a function type for executing actions
type ExecuteFunc func(context.Context, []string) (string, error)

// CompleteFunc is a function type for completing partial action commands
type CompleteFunc func([]string) ([]string, error)

// Action is an action that can be performed
type Action struct {

	// Name of the action
	Name string

	// Description of this action (optional)
	Description string

	// Execute is run that to perform the action
	Execute ExecuteFunc

	// Complete is an optional completion function for this action
	Complete CompleteFunc
}
