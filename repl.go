package httpr

import (
	"fmt"

	"github.com/abiosoft/ishell"
	"github.com/abiosoft/readline"
	"github.com/fatih/color"
)

// Repl starts a new "Read Eval Print Loop" (e.g. a prompt) with the
// provided Httper instance.
func Repl(h HTTPer) {
	shell := ishell.NewWithConfig(&readline.Config{
		Prompt: "$ ", // TODO make this support emoji without breaking autocomplete
	})

	shell.CustomCompleter(completer{h})
	shell.SetHomeHistoryPath(".httpr_history")

	for _, a := range h.actions {
		cmd := &ishell.Cmd{
			Name: a.Name,
			Help: a.Description,
			Func: executor(h, a),
		}

		shell.AddCmd(cmd)
	}

	color.Cyan(`
	 ▄ .▄▄▄▄▄▄▄▄▄▄▄ ▄▄▄·▄▄▄  
	██▪▐█•██  •██  ▐█ ▄█▀▄ █·
	██▀▐█ ▐█.▪ ▐█.▪ ██▀·▐▀▀▄ 
	██▌▐▀ ▐█▌· ▐█▌·▐█▪·•▐█•█▌
	▀▀▀ · ▀▀▀  ▀▀▀ .▀   .▀  ▀
	`)

	fmt.Printf("💥 HTTPR - the command line for teh interwebz 💥\n\n")

	shell.Run()
}

func executor(h HTTPer, a Action) func(c *ishell.Context) {
	return func(c *ishell.Context) {
		out, err := a.Execute(h.ctx, c.Args)
		if err != nil {
			c.Err(err)
			return
		}

		c.Println(out)
	}
}
