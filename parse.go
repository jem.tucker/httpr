package httpr

import (
	"errors"
	"strings"
)

func (r HTTPer) parse(cmd string) (*Action, error) {
	tokens := strings.Split(cmd, " ")
	if len(tokens) == 0 {
		return nil, errors.New("invalid command")
	}

	action, err := r.Action(tokens[0])
	if err != nil {
		return nil, err
	}

	return action, nil
}
