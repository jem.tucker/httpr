# HTTP'r

An HTTP REPL built in go and powered by [ishell](https://github.com/abiosoft/ishell).

## 🦪 `httpr`

The default `httpr` shell can be used to hit endpoints and test APIs.



## 🏗 Extending with custom actions

What if you want to add a new action? How about one that prints the time...

All that is required is a new action definition as shown below: 

```go
package main

import (
    "context"
    "time"

	"gitlab.com/jem.tucker/httpr"
	"gitlab.com/jem.tucker/httpr/action"
)

// Date prints the current date and time 🤷‍♂️
var Date = httpr.Action{
	Name: "date",
	Execute: func(c context.Context, s []string) (string, error) {
		time.Now().Format("Mon Jan 2 15:04:05")
	},
}

func main() {
	h := httpr.New(
		httpr.WithActions(
			action.Requestor,
            action.Exit,
            Time,
		),
	)

	httpr.Repl(h)
}
```