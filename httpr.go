package httpr

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/jem.tucker/httpr/internal/stringlist"
)

// HTTPer takes a command and executes it. Often this command will be an
// HTTP request to make.
type HTTPer struct {
	actions []Action

	ctx context.Context
}

// Option is a functional option type for the HTTPer constructor
type Option func(HTTPer) HTTPer

// New constructs a new `HTTPer` instance
func New(opts ...Option) HTTPer {
	h := HTTPer{}

	for _, opt := range opts {
		h = opt(h)
	}

	if h.ctx == nil {
		h.ctx = context.Background()
	}

	return h
}

// WithActions returns a new `Option` that will add all specified actions to the
// actions known to the new HTTPer
func WithActions(actions ...Action) Option {
	return func(h HTTPer) HTTPer {
		for _, a := range actions {
			h.actions = append(h.actions, a)
		}

		return h
	}
}

// WithContext returns a new `Option` that will set `ctx` as the context value
// on the new HTTPer
func WithContext(ctx context.Context) Option {
	return func(h HTTPer) HTTPer {
		h.ctx = ctx
		return h
	}
}

// Do executes a given command
func (h HTTPer) Do(cmd string) (string, error) {
	tokens := strings.Fields(cmd)
	if len(tokens) == 0 {
		return "", errors.New("invalid command")
	}

	name := tokens[0]
	rest := tokens[1:]

	action, err := h.Action(name)
	if err != nil {
		return "", err
	}

	return action.Execute(h.ctx, rest)
}

// Complete returns a nmber of suggestions for a given partial command string
func (h HTTPer) Complete(cmd string) ([]string, error) {
	tokens := strings.Fields(cmd)

	// If the command ends with a space we add an additional field the list
	// so the action completers know the correct thing to complete.
	if strings.HasSuffix(cmd, " ") {
		tokens = append(tokens, "")
	}

	switch len(tokens) {
	case 0:
		return h.ActionNames(), nil

	case 1:
		names := stringlist.StringList(h.ActionNames())
		return names.Select(func(s string) bool {
			return strings.HasPrefix(s, cmd)
		}), nil

	default:
		name := tokens[0]
		rest := tokens[1:]

		action, err := h.Action(name)
		if err != nil {
			return nil, err
		}

		if action.Complete == nil {
			return nil, nil
		}

		return action.Complete(rest)
	}
}

// Action returns the with the given name, or an error if one cannot be found
func (h HTTPer) Action(name string) (*Action, error) {
	for _, a := range h.actions {
		if a.Name != name {
			continue
		}

		return &a, nil
	}

	return nil, fmt.Errorf("unknown action: %s", name)
}

// ActionNames returns a slice containing the name of every registered action
func (h HTTPer) ActionNames() []string {
	var names []string
	for _, a := range h.actions {
		names = append(names, a.Name)
	}
	return names
}
